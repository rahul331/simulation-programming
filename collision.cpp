#include "collision.h"
#include "testplane.h"


Collision::Collision(Ball *b1, Ball *b2, double x){
    b[0]=b1;
    b[1]=b2;
    this->x=x;//position
    bw=false;
}

Collision::Collision(Ball *b1, TestPlane *w, double x){
    b[0]=b1;
    this->w=w;
    this->x=x;//position
    bw=true;
}


bool Collision::operator < (const Collision & other)const{
    return x < other.x;
}

// checking collision condition.......................................................

bool Collision::operator == (const Collision& other)const{
    if (b[0]== other.b[0]) return true;
    if (!other.bw && b[0]==other.b[1]) return true;
    if (!bw && b[1]== other.b[0]) return true;
    if (!bw && !other.bw && b[1] == other.b[1]) return true;
    return false;
}
