#include "controller.h"
#include "ball.h"
#include "testplane.h"


void Controller::localSimulate(double dt){
    // std::cout<<"dt"<<dt << std::endl;

    for (int i=0; i < _b.size(); i++)// filling up ball array
        _b[i]->update_step(dt);

    for (int i=0; i < _b.size(); i++)
        for (int j=i+1; j< _b.size(); j++)
            findBBcoll(_b[i], _b[j], coll);

    for (int i=0; i < _b.size(); i++)
        for (int j=0; j< _w.size(); j++)
            findBWcoll(_b[i], _w[j], coll);

// update colision for balls and wall..........................................

    while(coll.getSize() >0){
        coll.sort();
        coll.makeUnique();
        Collision co = coll[0];
        coll.removeFront();

        if(co.getIsBw()){// ball and wall
            co.getBalll(0)->moveBall(co.getX()*dt);
            colBW(co.getBalll(0), co.getWall(), (1-co.getX())*dt);// getX?

            for (int i = 0; i < _b.size(); i++ )
                if(_b[i] != co.getBalll(0))// getBalll?
                    findBBcoll(_b[i], co.getBalll(0), coll);

            for (int i = 0; i <_w.size(); i++ )
                if(_w[i] != co.getWall())
                    findBWcoll(co.getBalll(0), _w[i], coll);
        }

        else{// for ball- ball

            co.getBalll(0)->moveBall(co.getX()*dt);
            co.getBalll(1)->moveBall(co.getX()*dt);
            colBB(co.getBalll(0), co.getBalll(1),(1-co.getX())*dt);


            for(int i=0; i< _b.size(); i++)
                if(_b[i] != co.getBalll(0)){
                    findBBcoll(co.getBalll(0),_b[i], coll);
                    findBBcoll(co.getBalll(1),_b[i], coll);
                }

            for(int i=0; i< _w.size(); i++)
                if(_w[i] != co.getWall()){
                    findBWcoll(co.getBalll(0),_w[i],coll);
                    findBWcoll(co.getBalll(1),_w[i],coll);
                }
        }
    }
}

Controller::Controller(GMlib::PBezierSurf<float>* surf ){//ground_surf
    this->toggleDefaultVisualizer();
    this->replot(5,5,0,0);
    this->setVisible(false);
    this->surf=surf;//ground_surf
    this->insert(surf);
}

// find ball-ball collison........................................................................

void Controller::findBBcoll(Ball *b1, Ball *b2, GMlib::Array<Collision> &co){

    double y1 = b1->getX();
    double y2 = b2->getX();

    GMlib::Vector<float,3> h = (1+y1)*b1->get_ds() - (1+y2)*b2->get_ds();// difference between step vector
    GMlib::Point<float,3>  s = b1->getPos()- b2->getPos()- y1*b1->get_ds()+ y2*b2->get_ds(); //diff between position

    double a = h*h;
    double b = s*h;
    double r = b1->getRadius() + b2->getRadius();
    double c = s*s - r*r;
    double k = b*b-a*c;


    if (k>0)
    {
        if(std::abs(a)<1.0e-6){
            return;
        }

        double x= (-b-sqrt(k))/a;

        if (x > 0 && x <= 1){
            co += Collision(b1,b2,x);

        }
    }
}

//ball and wall collision..............................................................................

void Controller::findBWcoll(Ball *b, TestPlane *w, GMlib::Array<Collision> &co){

    GMlib::Point<float,3> positionBall = b->getPos();
    GMlib::Point<float,3> positionWall = w->getCornerPoint();
    float radius = b->getRadius();
    GMlib::Vector<float,3> d  = positionWall-positionBall ;//new position
    GMlib::Vector<float,3> n  = w->getNormal();// normal
    GMlib::Vector<float,3> ds = b->get_ds();

    double dn  = d*n;
    double dsn = ds*n;   // new step

    if(dn+radius>0.0)
    {//checking ball to move inside walls
        b->translate(1.5*(dn+radius)*n);
        dn = -0.5*dn - (1.5*radius);
        //std::cout<< "translate" << dn<< std::endl;
    }
    if (dsn < -1e-6 ){
        double x = (radius + dn)/dsn;

        if(x >0 && x <=1){
            co += Collision(b,w,x);
        }
    }
}

// ball and wall collision updating.......................................................................

void Controller::colBW(Ball *b, TestPlane *w, double dt_c){

    GMlib::Vector<float,3> new_vel = b->get_vel();
    new_vel -= 2*(b->get_vel()*w->getNormal())*w->getNormal();;
    b->set_v(new_vel);
    b->update_step(dt_c);
}

// Ball and ball collision updating........................................................................

void Controller::colBB(Ball *b1, Ball *b2, double dt_c){

    GMlib::Vector<float,3> _d  = b1->getPos() - b2->getPos();
    GMlib::Vector<float,3> v11 = b1->get_vel();
    GMlib::Vector<float,3> v22 = b2->get_vel();

    // std::cout << "v11: " << v11 <<  std::endl;

// set the new velocity for ball  and decompose vector............................................
    float dd = _d *_d;
    GMlib::Vector<float,3> vd11 = ((v11*_d)/dd)*_d;
    GMlib::Vector<float,3> vn11 = v11 -vd11;

    GMlib::Vector<float,3> vd22 = ((v22*_d)/dd)*_d;
    GMlib::Vector<float,3> vn22 = v22 -vd22;

    double mm = b1->getMass() - b2->getMass();
    double mp = b1->getMass() + b2->getMass();


    double omega1 = mm/mp;
    double omega2 = 2*b2->getMass()/mp;
    GMlib::Vector<float,3> v1 = omega1*vd11 + omega2*vd22 + vn11;

    b1->set_v(v1);
    b1->update_step(dt_c);


//set the velocity for ball ....................................................................

    double omega3 = -omega1;
    double omega4 = 2*b1->getMass()/mp;
    GMlib::Vector<float,3> v2 = omega3*vd22 + omega4*vd11 + vn22;

    b2->set_v(v2);
    b2->update_step(dt_c);
    //std::cout << "2-vf: " << v22 << " va: " << vd21 << std::endl;

}

