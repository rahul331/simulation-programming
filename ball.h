#ifndef TESTTORUS_H
#define TESTTORUS_H
#include"testplane.h"
#include <memory>

#include <parametrics/gmpsphere>


class Ball : public GMlib::PSphere<float> {

public:
    using PSphere::PSphere;

    Ball(GMlib::Vector<float,3> v, float m, float r, GMlib::PSurf<float,3>* s);
    ~Ball() {}

    void initPosition(const GMlib::Vector<float,3>& p);
    void set_v(GMlib::Vector<float,3> ve);

    const  GMlib::Vector<float,3>& get_ds()const;// step
    const  GMlib::Vector<float,3>& get_vel()const;// velocoty
    float  getMass() const;
    double getX() const;// position

    void   moveBall(double x);// ball position
    void   update_step(double dt);

protected:
    void localSimulate(double dt);

private:

    bool m_test01 {false};
    std::shared_ptr<Ball> ball_01 {nullptr};

    const GMlib::Point<float,3> pos;

    float m;     //mass
    double _dt;  // time step
    double x;

    GMlib::PSurf<float,3>* surf; // surface
    GMlib::Point<float,3> p;
    GMlib::Point<float,3> q;
    GMlib::Vector<float,3> vel;  // velocity
    GMlib::UnitVector<float,3> rot_x;// rotation vector

//coordinates
    float _u;
    float _v;

//normal
    GMlib::UnitVector<float,3> n;
    GMlib::Vector<float,3> ds;

};

#endif // BALL_H
