#ifndef COLLISION_H
#define COLLISION_H
#include "ball.h"
#include "testplane.h"

class Collision
{

private:
    Ball* b[2];// ball array
    TestPlane* w; // wall array
    double x;// position
    bool bw;

public:

    Collision(){}
    Collision(Ball* b1, Ball* b2, double x);// ball and ball
    Collision(Ball* b, TestPlane* w, double x);// ball and wall

    bool operator < (const Collision & other)const;
    bool operator == (const Collision& other)const;

inline
    Ball* getBalll(int i)const{
        return b[i];
    }
inline
    TestPlane* getWall()const{
        return w;
    }
inline
    double getX()const{
        return x;
    }
inline
    bool getIsBw()const{
        return bw;
    }

};

#endif // COLLISION_H
