#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "testplane.h"
#include "ball.h"
#include "collision.h"
#include "parametrics/gmpbeziersurf"

#include <core/gmarray>
#include <core/gmdmatrix>

class Controller:public GMlib::PSphere<float>{
    GM_SCENEOBJECT(Controller)

    private:

    GMlib::Array <Collision> coll;
    GMlib::Array<Ball*> _b;
    GMlib::Array<TestPlane*> _w;
    GMlib::PBezierSurf<float>* surf;

public:
    Controller(GMlib::PBezierSurf<float>* surf);//bezier surface

    inline
    void insertBall(Ball *b){
        this->insert(b);
        _b += b;
    }
    inline
    void insertTestPlane(TestPlane *w){
        this->insert(w);
        _w += w;
    }


protected:

    void findBBcoll(Ball *b1, Ball *b2, GMlib::Array<Collision> &co);
    void findBWcoll(Ball *b, TestPlane *w, GMlib::Array<Collision> &co);

    void  colBW(Ball *b, TestPlane *w, double dt_c);
    void  colBB(Ball *b1, Ball *b2, double dt_c);

    void localSimulate(double dt);

};

#endif // CONTROLLER_H
