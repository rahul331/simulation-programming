#ifndef TESTPLANE_H
#define TESTPLANE_H

#include <memory>

#include <parametrics/gmpplane>


class TestPlane : public GMlib::PPlane<float> {
public:
    using PPlane::PPlane;


    ~TestPlane() {

        if(m_test02)
            remove(test_02_ball.get());
    }

    const GMlib::Point<float,3>& getCornerPoint(){
        return this->_pt;
    }


private:
    bool m_test02 {false};
    std::shared_ptr<TestPlane> test_02_ball {nullptr};

};

#endif // TESTPLANE_H
