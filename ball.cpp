#include "ball.h"
#include "testplane.h"

Ball::Ball(GMlib::Vector<float,3> v, float m, float r, GMlib::PSurf<float,3>* s):GMlib::PSphere<float>(r){
    this->vel  = v;
    this->surf = s;
    this->m    = m;    // mass
              x= 0;    // postion
}

void Ball::initPosition(const GMlib::Vector<float,3>& p){

// finding closet point on plane....................................................................................

    surf->estimateClpPar( this->getPos(), _u, _v );
    surf->getClosestPoint(this->getPos(), _u , _v);

    GMlib::DMatrix<GMlib::Vector<float,3> > mat = surf->evaluate(_u,_v,1,1);
    n = mat[1][0] ^ mat[0][1];// bezier curve matrix

    GMlib::Vector<float,3> newpos = mat[0][0] + this->getRadius()*n;
    this->translate(newpos-getPos());
}

const GMlib::Vector<float,3>& Ball::get_ds()const{
    return ds;// step
}

const GMlib::Vector<float,3>& Ball::get_vel()const{
    return vel;
}

double Ball::getX()const{
    return x;
}

void Ball::set_v(GMlib::Vector<float,3> ve){
    vel=ve;
}

void Ball::moveBall(double x){
    translate(x*ds);
}

float Ball::getMass() const {
    return m;
}

// rolling ball........................................................................................................

void Ball::update_step(double dt){

    const GMlib::Vector<float,3> g(0.0, 0.0, -9.81);

// finding ds and closet point.........................................................................................

    ds = dt*vel + (0.5*dt*dt)*g;      // gravity
    p = this->getPos() + ds;          // point
    surf->getClosestPoint(p, _u, _v); // get closet point

    GMlib::DMatrix<GMlib::Vector<float,3> > m = surf->evaluate(_u,_v,1,1);
    n = m[0][1]^m[1][0];// corss product to calculate normal
    GMlib::Vector<float,3> newpos = m[0][0] + this->getRadius()*n;

//update velocity and ds and call translate..........................................................................

    double vv = vel*vel+2*(g*ds);// velocity old
    ds = newpos - this->getPos();
    vel  += dt*g;
    vel  -= (vel*n)*n;
    rot_x = n^ds;            // rotation of balls

    if(vv>0)
    {
        double nvv = vel*vel;// nvv is new velocity
        if(nvv>0.001)
            vel *= std::sqrt(vv/nvv);
    }
    else
    {
        double nvv = vel*vel;
        if(nvv>0.001)
            vel *= std::sqrt(-vv/nvv);
    }
}

void Ball::localSimulate(double dt){

    update_step(dt);
    this->translate(ds);
    this->rotate( ds.getLength()/getRadius(), rot_x );// rotation of ball
    x=0;
}
